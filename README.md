# OpenML dataset: optical_interconnection_network

https://www.openml.org/d/42365

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

``**Author**: Cigdem Inan Aci","Mehmet Fatih Akay  
**Source**: UCI - [original](http://archive.ics.uci.edu/ml/datasets/Optical+Interconnection+Network+) - Date unknown  
**Please cite**:   

*** Optical Interconnection Network Data Set***

### Data Set Information

All simulations have done under the software named OPNET Modeler. Message passing is used as the communication mechanism in which any processor can submit to the network a point-to-point message destined at any other processor. M/M/1 queue is considered in the calculations which consist of a First-in First-Out buffer with packet arriving randomly according to a Poisson arrival process, and a processor, that retrieves packets from the buffer at a specified service rate. In all simulations, it is assumed that the processor at each node extracts a packet from an input queue, processes it for a period of time and when that period expires, it generates an output data message. The size of each input queue is assumed as infinite. A processor becomes idle only when all its input queues are empty.

### Attribute Information 

1. Node Number: The number of the nodes in the network. (8x8 or 4x4).`
2. Thread Number: The number of threads in each node at the beginning of the simulation.`
3. Spatial Distribution: The performance of the network is evaluated using synthetic traffic workloads. Uniform (UN), Hot Region (HR), Bit reverse (BR) and Perfect Shuffle (PS) traffic models have been included.`
4. Temporal Distribution: Temporal distribution of packet generation is implemented by independent traffic sources. In our simulations, we utilized clientâ€“server traffic (i.e., a server node sends packets to respond to the reception of packets from clients) and asynchronous traffic (i.e., initially, all nodes generate traffic independently of the others; as time progresses, traffic generation at the source/destination nodes depends`
5. on the receipt of messages from destination/source nodes).`
6. T/R: Message transfer time (T ) Uniformly distributed with mean in range from 20 to 100 clock cycles. Thread run time (R) Exponentially distributed with a mean of 100 clock cycles.`
7. Processor Utilization: The average processor utilization measures the percent of time that threads are running in the processor.`
8. Channel Waiting Time: Average waiting time of a packet at the output channel queue until it is serviced by the channel.`
9. Input Waiting Time: Average waiting time of a packet until it is serviced by the processor.`
10. Network Response Time: The time between a request message is enqueued at the output channel and the corresponding data message is received in the input queue.`
11. Channel Utilization: The percent of time that the channel is busy transferring packets to the network.`

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/42365) of an [OpenML dataset](https://www.openml.org/d/42365). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/42365/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/42365/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/42365/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

